import axios from 'axios';

const config = axios.create ({
    baseURL: 'http://numbersapi.com/',
});

export default config;