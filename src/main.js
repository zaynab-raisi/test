import Vue from 'vue';
import App from './App.vue';
import store from './store/store';
import 'babel-polyfill';
import '../node_modules/tailwindcss/dist/tailwind.min.css';

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
