import config from '../../config/axiosConfig';
import clone from 'lodash/clone';

const types = {
    SET_FACT: 'SET_FACT',
    SET_NUMBER: 'SET_NUMBER'
};

const state = {
    fact: "",
    number: ""
};

const getters = {
    fact: (state) => state.fact,
    number: (state) => state.number,
    calculateNumber: (state, getters) => getters.number.toString().split('').reverse()
};

const mutations = {
    [types.SET_FACT](state, payload) {
        //state.fact = payload;
        state.fact = clone(payload);
    },

    [types.SET_NUMBER](state, payload) {
        state.number = payload;
    }
};

const actions = {
    async fetchFact({commit}, number) {
        await config.get(`${number}/math?json`)
        .then(respose => {
            console.log(respose);
            return respose.data;
        }).then(data => {
            commit(types.SET_NUMBER, data.number);
            commit(types.SET_FACT, { text: data.text});
        })
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}