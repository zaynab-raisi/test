import Vue from 'vue';
import Vuex from 'vuex';
import number from './modules/number';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {},
    modules: {
        number
    }
});

export default store;